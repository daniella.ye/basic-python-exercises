import sys
try:
    v0 = float(sys.argv[1])
    t = float(sys.argv[2])
except IndexError:
    v0 = eval( raw_input("Input the initial velocity:") )
    t = eval( raw_input("Input the time:") )
g = 9.81
t_min = 0
t_max = (2.0*v0/g)
if t >= t_min and t <= t_max:
    y = v0*t + 1/2.0*g*t**2
    print y
else:
    raise ValueError("your input time value is not between 0 to %0.1f" % (t_max))
