# Create a program that count number of specific words in the file, also total number of words

import sys 

word_count_of_word = 0
word_count = 0
file_name = "./data.text"

with open(file_name,'r') as file:
  for line in file:
    words_on_line = line.split()
    word_count += len(words_on_line)
    for word in words_on_line:
        if word == sys.argv[1]: word_count_of_word += 1 

print ("total number of word: \"%s\" %s" % ( sys.argv[1], word_count_of_word ) )
print ("total number of words: %s" % word_count)