import sys
try:
    F = float(sys.argv[1])
except:
    print "You failed to provide Fahrenheit degrees as input on the command line!"
    sys.exit(1)
    
C = 5.0/9*(F-32)
print "%.1fF is %.1fC" % (F, C)

