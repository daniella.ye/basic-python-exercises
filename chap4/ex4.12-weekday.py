import calendar
import sys

year, month, day = map(int, sys.argv[1:4])
weekday = calendar.weekday( year, month, day)
week_days_as_string = ("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")

print( week_days_as_string[weekday] )