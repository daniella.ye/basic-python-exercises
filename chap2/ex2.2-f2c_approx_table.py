# Write an approximate Fahrenheit–Celsius conversion table.
F = 0                                              
print "F\tC\tC2"                 
while F <= 100:                 
    C = (F - 32) * 5.0 / 9.0     
    C2 = (F - 30) / 2.0 
    print "%.1f\t%.1f\t%.1f" % (F, C, C2)     
    F = F + 10