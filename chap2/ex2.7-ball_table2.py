v0 = 1 
g = 9.81    
n = 11
start = 0.0
end = (2.0 * v0) / g
step = end / n
tList = []
yList = []
while start <= end:    
    tList.append(start)
    start += step 

for t in tList:    
    y = v0*t - 0.5*g*t**2
    yList.append(y)

for time, distance in zip(tList, yList):
    print ("time: %f\t distance: %f" %(time, distance)) 

