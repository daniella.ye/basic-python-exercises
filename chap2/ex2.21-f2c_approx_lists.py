F = []
C = []
C_approx = []

def f2c_approx(F):
    return (F - 30) / 2.0 
def f2c(F):
    return (F - 32) * 5.0 / 9.0

for i in range(0, 101, 10):
    F.append(i)
    C.append(f2c(i))
    C_approx.append(f2c_approx(i))

print "F\tC\tC_approx" 
total_list = [F, C, C_approx]     

for i in range(len(F)):
    print "\t"                      
    for j in total_list:
        print "%.1f\t" % j[i],
                
        