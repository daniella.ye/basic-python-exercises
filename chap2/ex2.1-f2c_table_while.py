# Make a Fahrenheit–Celsius conversion table.
F = 0                                              
print "F\tC"                 
while F <= 100:                 
    C = (F - 32) * 5.0 / 9.0      
    print "%.1f\t%.1f" % (F, C)     
    F = F + 10