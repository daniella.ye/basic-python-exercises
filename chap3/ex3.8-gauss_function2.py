import math
def gauss(x, m=0, s=1):
    return 1/(math.sqrt(2*math.pi)*s)*math.exp(-0.5*(float(x-m)/s)**2)
x_range = [round((x * 0.1), 2) for x in range(-50, 51)]
gauss_val = []
for i in x_range:
    gauss_val.append(gauss(i,0,1)) 
print gauss_val
