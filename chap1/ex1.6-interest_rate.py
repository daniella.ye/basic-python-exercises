# Compute the growth of money in a bank.
A = input('Enter the initial amount of money: ')
n = input('Enter years: ')
p = input('Enter the interest rate (in percent per year): ')
final_amount = A * (1 + p / 100.0) ** n
print("""
After %g years with %g%% interest rate, an initial amount %g euros have grown to %g.
""" % (n, p, A, final_amount))