# Compute the mass of various substances.

# densities for following substances (g/cm**3)
iron = 7.8
air = 0.0012
gasoline = 0.67
ice = 0.9
body = 1.03
silver = 10.5
platinum = 21.4

#volume (1 Liter = 1000 cm**3)
v_liter = 1
v_c = v_liter * 1000

print("""
From %g liter, the mass of 
iron        is %g grams
air         is %g grams
gasoline    is %g grams
ice         is %g grams
human body  is %g grams
silver      is %g grams
platinum    is %g grams
""" % (v_liter, v_c * iron, v_c * air, v_c * gasoline, v_c * ice, v_c * body, v_c * silver, v_c * platinum))
