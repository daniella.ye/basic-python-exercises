# Convert from meters to British length units.
meter = input('Enter the meter you want to convert: ')
inch = meter * 100 / 2.54
foot = inch / 12.0
yard = foot / 3.0
british_mile = yard / 1760.0
print 'A length of %.2f meters corresponds to %.2f inches, %.2f feet, %.2f yards, or %.4f miles. ' % (meter, inch, foot, yard, british_mile)
