# basic python programming exercises

Some solutions to the exercises posed by the book 'A Primer on Scientific Programming with Python, 2nd Edition (2011)'

If you disagree with the solution or have ideas for alternate solutions, please leave a message

## Why do this?
I'm doing a master degree in CS and one of my class is using the book. I have weekly homework which included some exercises in the text book.
I will continue finishing all the exercises as a practice, but for now I'll just post my homework here.
Hope it can be useful.
Also I'm a python beginner. Some solutions may not very write in a professional way, but I'll keep them being update.